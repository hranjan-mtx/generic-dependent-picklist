/**
 * Created by hardikranjan on 07/07/20.
 */

import { LightningElement, wire, api, track } from 'lwc';

export default class HowToUse extends LightningElement {

	@track selectedValues;


	handlePicklist(event) {
		let _selectedValues = event.detail.pickListValue;
		window.console.log('---- selectedValues --- ' + JSON.stringify(_selectedValues));
		this.selectedValues = JSON.parse(JSON.stringify(_selectedValues));

	}
}